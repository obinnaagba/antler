/**
 * Created by teliov on 12/27/16.
 */
var gulp = require('gulp');
var order = require("gulp-order");
var concat = require('gulp-concat');
var replace = require('gulp-replace-task');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var argv = require('yargs').argv;
var sass = require('gulp-sass');
var path = require('path');

var destinationDir =  path.resolve(__dirname +'/./app/assets/');
var sourceDir = path.resolve(__dirname +'/./src/');
var env;

if (argv.production){
  env = require('./environment/prod.json');
}else{
  env = require('./environment/dev.json');
}

var paths = {
  sass: [
    './bower_components/bootstrap-3-card/sass/_card.scss'
  ],

  css: [
    './bower_components/bootstrap/dist/css/bootstrap.css',
    './bower_components/fontawesome/css/font-awesome.css',
    './bower_components/animate.css/animate.css',
    './bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css',
    './bower_components/ngprogress/ngprogress.css',
    './bower_components/ng-tags-input/ng-tags-input.css',
    './bower_components/sweetalert/dist/sweetalert.css',
    './bower_components/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css'
  ],

  libJs: [
    './bower_components/lodash/dist/lodash.js',
    './bower_components/moment/moment.js',
    './bower_components/spin.js/spin.js',
    './bower_components/sprintf/dist/sprintf.min.js',
    './src/vendor/rrule.js',
    './bower_components/chart.js/dist/Chart.js',
    './bower_components/jquery-mousewheel/jquery.mousewheel.js',
    './src/vendor/jquery.mCustomScrollbar.js',
    './bower_components/bootstrap/dist/js/bootstrap.js',
    './bower_components/angular/angular.js',
    './bower_components/angular-promise-extras/angular-promise-extras.js',
    './bower_components/angular-ui-router/release/angular-ui-router.js',
    './bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
    './bower_components/angular-chart.js/dist/angular-chart.js',
    './bower_components/angular-cache/dist/angular-cache.js',
    './bower_components/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.js',
    './bower_components/angular-moment/angular-moment.js',
    './bower_components/ng-scrollbars/src/scrollbars.js',
    './bower_components/angular-spinner/dist/angular-spinner.js',
    './src/vendor/angular-pouch.js',
    './src/vendor/angular-messages.js',
    './bower_components/ngprogress/build/ngprogress.js',
    './bower_components/ng-tags-input/ng-tags-input.js',
    './bower_components/sweetalert/dist/sweetalert.min.js',
    './src/vendor/SweetAlert.js'
  ],

  srcJs: [
    './src/js/**/*'
  ],

  fonts: [
    './bower_components/fontawesome/fonts/**/*',
    './bower_components/bootstrap/fonts/**/*'
  ],
  images: [
    './src/images/**/*'
  ]
};

gulp.task('copy', function () {
  // copy bootstrap fonts
  gulp.src(paths.fonts)
    .pipe(gulp.dest(destinationDir + '/fonts'));

  gulp.src(paths.images)
    .pipe(gulp.dest(destinationDir + '/images'));


  gulp.src([
    './src/css/**/*'
  ])
    .pipe(gulp.dest(destinationDir + '/css/'));

  gulp.src([
    './src/templates/**/*'
  ])
    .pipe(gulp.dest(destinationDir + '/templates/'));

  gulp.src('./src/index.html')
    .pipe(gulp.dest('./app'));
});

gulp.task('css', function(){
  var css = gulp.src(paths.css)
    .pipe(concat('lib.css'));

  if (env.environment == "production"){
    css = css.pipe(minifyCss({
      keepSpecialComments: 0
    }));
  }

  css.pipe(rename({extname: '.min.css'}))
    .pipe(gulp.dest(destinationDir + '/css/'));
});

gulp.task('sass', function(){
  gulp.src(paths.sass)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(sourceDir));
});

gulp.task('libJs', function(){
  var js = gulp.src(paths.libJs)
    .pipe(gulp.dest(destinationDir + '/js/'))
});

gulp.task('srcJs', function(){
  gulp.src(paths.srcJs)
    .pipe(order([
      'app/*.js',
      '**/index.js'
    ]))
    .pipe(concat('app.min.js'))
    .pipe(gulp.dest(destinationDir+ '/js/'))
});

gulp.task('replace', function(){
  gulp.src('./src/constants.js')
    .pipe(replace({
      patterns: [
        {
          match: 'ENV',
          replacement: env.environment
        },
        {
          match: 'POUCHDB_URL',
          replacement: env.pouch_url
        },
        {
          match: 'POUCHDB_USER_URL',
          replacement: env.pouch_user_url
        },
        {
          match: 'POUCHDB_USER',
          replacement: env.pouch_user
        },
        {
          match: 'POUCHDB_PASSWORD',
          replacement: env.pouch_password
        }
      ]
    }))
    .pipe(gulp.dest('./src/js/commons'));
});

gulp.task('default', ['replace','sass', 'css','libJs','srcJs', 'copy']);
