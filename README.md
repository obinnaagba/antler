# Running Instructions

- To run locally, you would need to install pouchdb-server for running a local couchddb server

- Run gulp to build

- Then :

```bash
cd app/
electron .
```

# Build Instructions

## Linux 64 Bit
```bash
./node_modules/.bin/build --linux --x64
```

## Linux 32 Bit
```bash
./node_modules/.bin/build --linux --ia32
```

## Windows 64 Bit
```bash
.\node_modules\.bin\build.cmd --win --x64
```

## Windows 32 Bit
```bash
.\node_modules\.bin\build.cmd --win --ia32
```


## N.B:
The tricky thing with building for different platforms is that you need to be on that platform to build for that platform.

So if you need a Linuz 64 bit build, then you'll need to be on a linux 64 bit PC and so on