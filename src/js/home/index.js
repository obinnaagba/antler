/**
 * Created by teliov on 3/29/17.
 */
(function () {
  angular.module('app.home', ['app.commons', 'ui.bootstrap', 'mwl.calendar'])
    .controller('HomeCtrl', ['$scope', '$state', 'calendarConfig', function ($scope, $state, calendarConfig) {


        $scope.calendarView = "month";
        $scope.viewDate = new Date();
        $scope.events = [];

        var actions = [{
          label: '<i class=\'fa fa-calendar-o\'></i>',
          onClick: function (args) {
            var calendarEvent = args.calendarEvent;
            switch (calendarEvent.type){
              case "intention":
                $state.go("app.intentions_list", {date: calendarEvent.startsAt});
                break;
              case "birthday":
                $state.go("app.birthdays_list", {date: calendarEvent.startsAt});
                break;
              case "wedding":
                $state.go("app.weddings_list", {date: calendarEvent.startsAt});
                break;
              case "thanksgiving":
                $state.go("app.thanksgiving_list", {date: calendarEvent.startsAt})
                break;
            }
          }
        }];

        var events = [
          {
            title: 'Mass Intentions',
            color: calendarConfig.colorTypes.warning,
            type: "intention",
            actions: actions,
            rrule: {
              freq: RRule.DAILY,
              byhour: 6,
              byminute: 0,
              bysecond: 0
            }
          },
          {
            title: 'Thanksgivings',
            color: calendarConfig.colorTypes.info,
            type: "thanksgiving",
            actions: actions,
            rrule: {
              freq: RRule.DAILY,
              byhour: 7,
              byminute: 0,
              bysecond: 0
            }
          },
          {
            title: 'Birthdays',
            color: calendarConfig.colorTypes.success,
            type: "birthday",
            actions: actions,
            rrule: {
              freq: RRule.DAILY,
              byhour: 8,
              byminute: 0,
              bysecond: 0
            }
          },
          {
            title: 'Wedding Anniversaries',
            color: calendarConfig.colorTypes.inverse,
            type: "wedding",
            actions: actions,
            rrule: {
              freq: RRule.DAILY,
              byhour: 9,
              byminute: 0,
              bysecond: 0
            }
          }
        ];

        $scope.onCalendarViewChanged = function () {
          return false;
        };

        $scope.$watchGroup([
          'calendarView',
          'viewDate'
        ], function () {
          $scope.events = [];

          events.forEach(function (event) {

            // Use the rrule library to generate recurring events: https://github.com/jkbrzt/rrule
            var rule = new RRule(angular.extend({}, event.rrule, {
              dtstart: moment($scope.viewDate).startOf($scope.calendarView).toDate(),
              until: moment($scope.viewDate).endOf($scope.calendarView).toDate()
            }));

            rule.all().forEach(function (date) {
              $scope.events.push(angular.extend({}, event, {
                startsAt: new Date(date)
              }));
            });

          });

        });
      }])
    .controller('BirthdaysListCtrl', ['$scope', '$state', '$stateParams', 'myAppDb', 'myAppProgress', 'myAppPopup'
      ,function($scope, $state, $stateParams, myAppDb, myAppProgress, myAppPopup){

        $scope.members = [];
        var date = $stateParams.date ? moment($stateParams.date): moment();
        var month = date.month() + 1;
        var day = date.date();
        $scope.title = date.format("dddd, MMMM Do YYYY");

        var db = myAppDb.getPouch();

        var loadView = function(){
          myAppProgress.start();
          db.find({
            selector: {
              _id: {
                $gt: null,
                $regex: /^[0-9]/
              }
            }
          })
            .then(function(res){
              var members = []
              res.docs.forEach(function(doc){
                if (parseInt(doc.birth_month, 10) == month && parseInt(doc.birth_day) == day){
                  members.push(doc)
                }
              });
              $scope.members = members;
            })
            .catch(function(err){
              console.log(err);
              myAppPopup.alert({
                title: "Oops!",
                body: "Unable to get the birthday celebrants for this month"
              });
            })
            .finally(function(){
              myAppProgress.stop();
            })
        }

        loadView();
    }])
    .controller('WeddingsListCtrl', ['$scope', '$state', '$stateParams', 'myAppDb', 'myAppProgress', 'myAppPopup'
      ,function($scope, $state, $stateParams, myAppDb, myAppProgress, myAppPopup){

        $scope.members = [];
        var date = $stateParams.date ? moment($stateParams.date) : moment();
        var month = date.month() + 1;
        var day = date.date();
        $scope.title = date.format("dddd, MMMM Do YYYY");

        var db = myAppDb.getPouch();

        var loadView = function(){
          myAppProgress.start();
          db.query('my_sacrament_index/by_sacraments', {
            key: "marriage",
            include_docs: true
          })
            .then(function(res){
              console.log(res);
              var members = [];
              res.rows.forEach(function(row){
                var sacrament = row.doc.sacraments.filter(function(sac){
                  return sac.id == "4"
                });
                sacrament = sacrament[0];

                if (parseInt(sacrament.month_of_reception) == month && parseInt(sacrament.day_of_reception) == day) {
                  members.push(row.doc)
                }
              });
              $scope.members = members;
            })
            .catch(function(err){
              console.log(err);
              myAppPopup.alert({
                title: "Oops!",
                body: "Unable to get the wedding anniversaries for this month"
              });
            })
            .finally(function(){
              myAppProgress.stop();
            })
        };

        loadView();
      }])
})();
