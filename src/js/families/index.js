(function () {
  angular.module('app.families', ['app.commons', 'ngTagsInput', 'oitozero.ngSweetAlert', 'ngMessages'])
    .controller('FamilyRegCtrl', ['$scope', 'myAppUtils', 'myAppDb', 'myAppPopup', 'myAppProgress', '$state', function ($scope, myAppUtils, myAppDb, myAppPopup, myAppProgress, $state) {

      $scope.title = "New Family Registration";
      $scope.member = {};

      $scope.register = function (regForm) {
        if (regForm.$invalid) return null;

        myAppProgress.reset();

        var children = $scope.member.children ? $scope.member.children.map(function (item) {
          return item.text;
        }) : [];
        var dependents = $scope.member.dependents ? $scope.member.dependents.map(function (item) {
          return item.text;
        }) : [];

        var obj = angular.extend({}, $scope.member);
        obj.children = children;
        obj.dependents = dependents;

        obj._id = "family_" + new Date().toJSON() + myAppUtils.machineId.machineIdSync();

        myAppProgress.start();
        myAppDb.getPouch()
          .put(obj)
          .then(function (doc) {
            myAppPopup.open({
              title: "Success",
              body: "Registration was successful"
            });
            myAppProgress.complete();
            $state.go($state.current, {}, {reload: true});
          })
          .catch(function (error) {
            console.log(error);
            myAppProgress.complete();
            myAppPopup.alert({
              title: "Error",
              body: "Registration was not successful"
            });
            myAppProgress.complete();
          })
      }
    }])
    .controller("FamilyRegEditCtrl", ['$scope', 'myAppUtils', 'myAppDb', 'myAppPopup', 'myAppProgress', '$state',
      '$stateParams', function ($scope, myAppUtils, myAppDb, myAppPopup, myAppProgress, $state, $stateParams) {

        $scope.title = "Edit Family Registration";
        $scope.member = {};
        myAppProgress.reset();

        myAppProgress.start();
        myAppDb.getPouch()
          .get($stateParams.familyId)
          .then(function (res) {
            $scope.member = res;
            $scope.member.children = res.children.map(function (child) {
              return {text: child};
            });
            $scope.member.dependents = res.dependents.map(function (dependent) {
              return {text: dependent}
            });
            myAppProgress.complete();
          })
          .catch(function (error) {
            console.log(error);
            myAppProgress.complete();
            myAppPopup.alert({
              title: "Error",
              body: "Unable to load family details for edit"
            });
            $state.go("app.families", {}, {reload: true});
          });

        $scope.register = function (regForm) {
          if (regForm.$invalid) return null;

          myAppProgress.reset();
          myAppProgress.start();

          var children = $scope.member.children.map(function (item) {
            return item.text;
          });
          var dependents = $scope.member.dependents.map(function (item) {
            return item.text;
          });

          var obj = angular.extend({}, $scope.member);
          obj.children = children;
          obj.dependents = dependents;

          myAppDb.getPouch()
            .put(obj)
            .then(function (doc) {
              myAppPopup.open({
                title: "Success",
                body: "Registration was successful"
              });
              myAppProgress.complete();
              $state.go($state.current, {}, {reload: true});
            })
            .catch(function (error) {
              console.log(error);
              myAppProgress.complete();
              myAppPopup.alert({
                title: "Error",
                body: "Registration was not successful"
              });
              myAppProgress.complete();
            })
        }
      }])
    .controller('FamiliesCtrl', ['$scope', 'myAppDb', 'myAppPopup','myAppProgress', function ($scope, myAppDb, myAppPopup, myAppProgress) {
      $scope.families = [];
      $scope.searchField = "";

      var defaultFn = function () {
        myAppProgress.start();
        myAppDb.getPouch()
          .find({
            selector: {
              _id: {
                "$gt": null,
                "$regex": "^family_"
              }
            }
          })
          .then(function (res) {
            myAppProgress.complete();
            $scope.families = res.docs;
          })
          .catch(function (error) {
            myAppProgress.complete();
            myAppPopup.alert({
              title: "Error",
              body: "Unable to fetch families"
            })
            console.log(error);
          })
      };

      $scope.doSearch = function () {
        if (!$scope.searchField) return defaultFn();
        myAppProgress.start();
        var regex = new RegExp($scope.searchField, "i");
        myAppDb.getPouch()
          .find({
            selector: {
              _id: {
                $gt: null
              },
              family_name: {
                $regex: regex
              }
            }
          })
          .then(function (res) {
            myAppProgress.complete();
            if (res.docs.length == 0) {
              myAppPopup.alert({
                title: "Oops!",
                body: "No results found"
              });
            } else {
              $scope.families = res.docs;
            }
          })
          .catch(function (error) {
            myAppProgress.complete();
            myAppPopup.alert({
              title: "Error",
              body: "Unable to complete search"
            });
            console.log(error);
          })
      };

      $scope.clearSearch = function(){
        defaultFn();
      };

      $scope.deleteFamily = function(family){
        myAppPopup.confirm({
          title: "Are you sure?",
          body: "Do you really want to delete this family, this action cannot be undone!",
          callback: function(ok){
            if (!ok) return;
            var db = myAppDb.getPouch();
            family = angular.extend(family, {
              _deleted: true
            });
            db.put(family)
              .then(function(){
                loadView();
                myAppPopup.open({
                  title: "Success",
                  body: "Family was successfully deleted"
                })
              })
              .catch(function(err){
                myAppPopup.alert({
                  title: "Oops!",
                  body: "Something went wrong while deleting this record"
                });
              })
          }
        })
      };


      function loadView () {
        defaultFn();
      }

      // kick things off
      loadView();
    }])
    .controller('FamilyCtrl', ['$scope', '$state', 'myAppDb', 'myAppPopup', 'myAppProgress', '$stateParams',
      function ($scope, $state, myAppDb, myAppPopup, myAppProgress, $stateParams) {
        $scope.title = "Family Detail";
        myAppProgress.reset();
        myAppProgress.start();

        var initiateFamily  = function(res){
          $scope.family = res;
          myAppProgress.complete();
        };

        myAppDb.getPouch().get($stateParams.familyId)
          .then(function(res){
            initiateFamily(res);
            myAppProgress.complete();
          })
          .catch(function(err){
            console.log(err);
            myAppProgress.complete();
            myAppPopup.alert({
              title: "Error",
              message: "Unable to fetch family details"
            });
            $state.go("app.families", {}, {reload: true})
          })
      }])
})();
