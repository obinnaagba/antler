/**
 * Created by teliov on 12/27/16.
 */
(function () {
  'use strict';
  var machineId = require('node-machine-id');
  var moment = require('moment');
  var json2xls = require('json2xls');

  angular.module('app.commons', ['ui.bootstrap', 'ngScrollbars', 'angularSpinner', 'pouchdb', 'ngPromiseExtras', 'angular-cache'])
    .factory('myAppPopup', ['$uibModal', '$rootScope', function ($uibModal, $rootScope) {
      var MODAL_TPL =
        "<div class='modal-header'>" +
        "<h3 class='modal-title' >{{ title }}</h3>" +
        "</div>" +
        "<div class='modal-body' > {{ body }}</div>" +
        "<div class='modal-footer'>" +
        "<button class='btn' ng-repeat='button in buttons' ng-class='button.class || \"btn-primary\"' type='button' ng-click='buttonTapped(button, $event)'>{{ button.text }}</button>" +
        "</div>";

      var opts = {
        controller: function ($scope, $uibModalInstance) {
          $scope.buttonTapped = function () {
            $uibModalInstance.close();
          }
        },
        template: MODAL_TPL,
        size: "sm"
      };

      return {
        open: function (scopeOpts) {
          scopeOpts = scopeOpts || {};
          if (!scopeOpts.buttons) {
            scopeOpts.buttons = [
              {
                class: 'btn-success',
                text: "OK"
              }
            ];
          }

          var scope = $rootScope.$new();
          angular.extend(scope, scopeOpts);
          var options = angular.extend({}, opts);
          options.scope = scope;
          $uibModal.open(options)
        },

        alert: function (scopeOpts) {
          scopeOpts = angular.extend({}, scopeOpts, {
            buttons: [{
              class: 'btn-danger',
              text: "Dismiss"
            }]
          });
          this.open(scopeOpts);
        },

        confirm: function (scopeOpts) {
          scopeOpts = angular.extend({}, scopeOpts, {
            buttons: [
              {
                class: 'btn-primary',
                text: "Confirm",
                type: "ok"
              },
              {
                class: 'btn-default',
                text: "Cancel",
                type: "cancel"
              }
            ]
          });

          var opts = {
            controller: function ($scope, $uibModalInstance) {
              $scope.buttonTapped = function (button) {
                var isOk = button.type == "ok";
                if ($scope.callback) {
                  $scope.callback(isOk);
                }
                $uibModalInstance.close();
              }
            },
            template: MODAL_TPL,
            size: "sm"
          };

          var scope = $rootScope.$new();
          angular.extend(scope, scopeOpts);
          var options = angular.extend({}, opts);
          options.scope = scope;
          $uibModal.open(options)
        }
      }

    }])
    .factory('myAppUtils', [function () {
      return {
        machineId: machineId,
        moment: moment
      }
    }])
    .service('myAppProgress', ['usSpinnerService', '$timeout', function (spinner, $timeout) {

      return {
        start: function () {
          $timeout(function () {
            spinner.spin()
          }, 100);
        },

        complete: function () {
          $timeout(function () {
            spinner.stop()
          }, 100);
        },

        stop: function () {
          $timeout(function () {
            spinner.stop()
          }, 100);
        },

        reset: function () {
          $timeout(function () {
            spinner.stop()
          }, 100);
        }
      }
    }])
    .provider('myAppDb', ['pouchDBProvider', 'POUCHDB_METHODS', '$injector', function (pouchDBProvider, POUCHDB_METHODS, $injector) {
      var dbPath, userDbPath, remotePath, remoteUserPath, username, password, localDb, localUserDb, remoteDb, remoteUserDb, syncHandler, userSyncHandler, pouchDB, hasBeenInit = false;


      this.init = function (opt) {
        // ADD EXTENSIONS TO ANGULAR POUCHDB FOR pouchdb-find
        var findMethods = {
          createIndex: "qify",
          getIndexes: "qify",
          deleteIndex: "qify",
          find: "qify"
        };

        pouchDBProvider.methods = angular.extend({}, POUCHDB_METHODS, findMethods);

        if (!opt.localPath) throw new Error("Local Path must be provided");
        dbPath = opt.localPath;

        if (!opt.userLocalPath) throw new Error("User Local Path must be provided");
        userDbPath = opt.userLocalPath;

        if (!opt.remoteUrl) throw new Error("Remote URL Required");
        remotePath = opt.remoteUrl;

        if (!opt.remoteUserUrl) throw new Error("Remote User URL Required");
        remoteUserPath = opt.remoteUserUrl;

        if (opt.hasOwnProperty('username') && opt.hasOwnProperty('password')) {
          username = opt.username;
          password = opt.password;
        }
      };

      var organizationDesignDoc = {
        _id: "_design/my_organization_index",
        views: {
          by_organization: {
            map: function (doc) {
              if (doc.organizations && doc.organizations.length > 0) {
                doc.organizations.forEach(function (org) {
                  emit(org.abrev);
                });
              }
            }.toString()
          }
        }
      };

      var sacramentsDesignDoc = {
        _id: "_design/my_sacrament_index",
        views: {
          by_sacraments: {
            map: function (doc) {
              if (doc.sacraments && doc.sacraments.length > 0) {
                doc.sacraments.forEach(function (sacrament) {
                  emit(sacrament.name);
                });
              }
            }.toString()
          }
        }
      };

      var ageDesignDoc = {
        _id: "_design/my_age_index",
        views: {
          by_age: {
            map: function (doc) {
              if (doc.age_group) {
                emit(doc.age_group.id);
              }
            }.toString()
          }
        }
      };

      var genderDesignDoc = {
        _id: "_design/my_gender_index",
        views: {
          by_gender: {
            map: function (doc) {
              if (doc.gender) {
                emit(doc.gender);
              }
            }.toString()
          }
        }
      };

      var stateDesignDoc = {
        _id: "_design/my_state_index",
        views: {
          by_state: {
            map: function (doc) {
              if (doc.state_of_origin) {
                emit(doc.state_of_origin);
              }
            }.toString()
          }
        }
      };

      var familyNameDesignDoc = {
        _id: "_design/family_name_index",
        views: {
          by_family_name: {
            map: function (doc) {
              if (/^family_/.test(doc._id) && doc.family_name) {
                emit(doc.family_name)
              }
            }.toString()
          }
        }
      };

      var usernameDesignDoc = {
        _id: "_design/my_username_index",
        views: {
          by_username: {
            map: function (doc) {
              if (doc.username) {
                emit(doc.username);
              }
            }.toString()
          }
        }
      };

      var initDB = function (pouchDB) {
        var PouchDB = require('pouchdb-node');
        PouchDB.plugin(require('pouchdb-find'));
        PouchDB.plugin(require('pouchdb-quick-search'));

        // db for admin users
        var adminUsers = new PouchDB(userDbPath);
        var remoteUserOpt = {};
        if (username && password) {
          remoteUserOpt.auth = {
            username: username,
            password: password
          }
        }
        var remoteAdminUsers = new PouchDB(remoteUserPath, remoteUserOpt);
        localUserDb = pouchDB(adminUsers);
        remoteUserDb = pouchDB(remoteAdminUsers);
        userSyncHandler = localUserDb.sync(remoteUserDb, {
          live: true,
          retry: true,
          filter: function (doc) {
            return doc._id.substr(0, 7) !== '_design';
          }
        });

        // build index for searching
        localUserDb.search({
          fields: ['username'],
          build: true
        }).then(function (info) {
        }).catch(function (err) {
        });

        // build index for searching by username
        localUserDb.get(usernameDesignDoc._id)
          .then(function () {
          })
          .catch(function (err) {
            if (err.error && err.status == 404) {
              localUserDb.put(usernameDesignDoc);
            }
          });


        // db for members
        var l = new PouchDB(dbPath);
        var remoteOpt = {};
        if (username && password) {
          remoteOpt.auth = {
            username: username,
            password: password
          }
        }

        var r = new PouchDB(remotePath, remoteOpt);
        localDb = pouchDB(l);
        remoteDb = pouchDB(r);

        syncHandler = localDb.sync(remoteDb, {
          live: true,
          retry: true,
          filter: function (doc) {
            return doc._id.substr(0, 7) !== '_design';
          }
        });

        // build index for searching
        localDb.search({
          fields: ['fullname', 'email', 'occupation', 'phone_number', 'gender'],
          build: true
        }).then(function (info) {
        }).catch(function (err) {
        });

        // build index for searching by organization
        localDb.get(organizationDesignDoc._id)
          .then(function () {
          })
          .catch(function (err) {
            if (err.error && err.status == 404) {
              localDb.put(organizationDesignDoc);
            }
          });

        // build index for searching by sacraments
        localDb.get(sacramentsDesignDoc._id)
          .then(function (res) {
          })
          .catch(function (err) {
            if (err.error && err.status == 404) {
              localDb.put(sacramentsDesignDoc);
            }
          });

        // build index for searching by age_group
        localDb.get(ageDesignDoc._id)
          .then(function (res) {
          })
          .catch(function (err) {
            if (err.error && err.status == 404) {
              localDb.put(ageDesignDoc);
            }
          });

        // build index for searching by gender
        localDb.get(genderDesignDoc._id)
          .then(function (res) {
          })
          .catch(function (err) {
            if (err.error && err.status == 404) {
              localDb.put(genderDesignDoc);
            }
          });

        // build index for searching by state
        localDb.get(stateDesignDoc._id)
          .then(function (res) {
          })
          .catch(function (err) {
            if (err.error && err.status == 404) {
              localDb.put(stateDesignDoc);
            }
          });

        // build index for searching by family_name
        localDb.get(familyNameDesignDoc._id)
          .then(function (res) {
          })
          .catch(function (err) {
            if (err.error && err.status == 404) {
              localDb.put(familyNameDesignDoc);
            }
          });

        return {
          member: syncHandler,
          admin: userSyncHandler
        };
      };

      this.$get = ['pouchDB', function (pouchDB) {

        if (!hasBeenInit) {
          initDB(pouchDB);
          hasBeenInit = true;
        }
        return {
          getSyncHandler: function () {
            return syncHandler;
          },

          getAdminSyncHandler: function(){
            return userSyncHandler;
          },

          getAdminPouch: function(){
            return localUserDb;
          },

          getRemoteAdminPouch: function(){
            return remoteUserDb;
          },

          getPouch: function () {
            return localDb
          },

          getRemotePouch: function () {
            return remoteDb;
          },

          searchAdminByUsername: function(options) {
            return localUserDb.query('my_username_index/by_username', options);
          },

          allDocs: function (options) {
            if (!options) options = {};
            return localDb.query('my_id_index/by_id', options)
          },

          searchByName: function (options) {
            return localDb.query('my_fullname_index/by_fullname', options);
          },

          searchByGender: function (options) {
            return localDb.query('my_gender_index/by_gender', options)
          },

          searchBySacrament: function (options) {
            return localDb.query('my_sacrament_index/by_sacraments', options);
          },

          searchByOrganizations: function (options) {
            return localDb.query('my_organization_index/by_organization', options);
          },

          searchByOccupation: function (options) {
            return localDb.query('my_occupation_index/by_occupation', options);
          },

          searchByAge: function (options) {
            return localDb.query('my_age_index/by_age', options);
          },

          searchByState: function (options) {
            return localDb.query('my_state_index/by_state', options);
          }
        };
      }]
    }])
    .filter('ucfirst', function () {
      return function (input) {
        input = input || '';
        var parts = input.split(" ") || [];
        for (var idx = 0; idx < parts.length; idx++) {
          if (parts[idx].length <= 0) continue;
          var temp = parts[idx].toLowerCase();
          parts[idx] = temp[0].toUpperCase() + temp.slice(1);
        }

        return parts.join(" ");
      }
    })
    .controller('BaseCtrller', ['$scope', '$state', function ($scope, $state) {
      (function () {
        var element = angular.element('body');
        if (!element.hasClass('nav-md')) element.addClass('nav-md');
        if (element.hasClass('login')) element.removeClass('login');
      })();

      $scope.scrollConfig = {
        autoHideScrollbar: true,
        theme: 'minimal',
        mouseWheel: {preventDefault: true}
      };

      $scope.doLogout = function () {
        $state.go('login', {}, {location: "replace", reload: true});
      }
    }])
    .factory('myAppSms', ['$http', '$q', '$httpParamSerializer', 'BULKSMS_USERNAME', 'BULKSMS_PASSWORD'
      , function ($http, $q, $httpParamSerializer, bulkSmsUsername, bulkSmsPassword) {
        var url = "http://bulksmsinnigeria247.com/components/com_spc/smsapi.php";
        var username = bulkSmsUsername;
        var password = bulkSmsPassword;
        var smsHeader = "SACCAKOKA";
        return {
          sendSms: function (message, recipients) {
            recipients = _.filter(recipients, function (val) {
              return (/^234/.test(val) && val.length == 13) || (/^0/.test(val) && val.length == 11);
            });

            if (recipients.length == 0) return $q.reject({
              error: "Insufficient number of valid recipients"
            });

            var numOfRecipients = recipients.length;

            function promiseSpitter(recipients, message) {
              var q = $q.defer();
              var data = {
                recipient: recipients.join(","),
                username: username,
                password: password,
                sender: smsHeader,
                message: message
              };
              data = $httpParamSerializer(data);
              var req = {
                method: 'POST',
                url: url,
                data: data,
                headers: {
                  'content-type': 'application/x-www-form-urlencoded; charset=utf-8'
                }
              };

              $http(req)
                .then(function (response) {
                  console.log(response);
                  var msg = response.data.split(/\s+/);
                  if (msg.length > 0 && msg[0].toLowerCase() == "ok") {
                    var failedMessages = msg[2].trim();
                    var failedSms = failedMessages == "" ? 0 : parseInt(failedMessages.split(",").length, 10);
                    var successSms = numOfRecipients - failedSms;
                    return q.resolve({
                      message: message,
                      recipients: successSms,
                      status: "Message sent successfully",
                      credits: msg[1],
                      failed: failedSms
                    });
                  } else {
                    q.reject({
                      error: "Message Sending Failed",
                      error_code: msg,
                      failed: numOfRecipients
                    })
                  }
                })
                .catch(function (err) {
                  console.log(err);
                  q.reject({
                    error: "Message Sending Failed",
                    reason: err,
                    failed: numOfRecipients
                  });
                });

              return q.promise;
            }

            var promises = [];

            while (recipients.length > 0) {
              var chunk = recipients.slice(0, 100);
              recipients = recipients.slice(100);
              promises.push(promiseSpitter(chunk, message));
            }

            return $q.allSettled(promises)
              .then(function (results) {
                console.log(results);
                var successes = [];
                var errors = [];
                results.forEach(function (res) {
                  if (res.state == "fulfilled") {
                    successes.push(res.value);
                  } else {
                    errors.push(res.value);
                  }
                });

                return {
                  successes: successes,
                  errors: errors
                }
              });
          }
        }
      }])
    .factory('myAppExcelExport', ['$q', function($q){
      var service = {};

      var dialog = require('electron').remote.dialog;
      var fs = require('fs');

      var fieldNames = [
      'reg_no', 'firstname', 'lastname', 'phone_number', 'email', 'phone_number_2',
      'address', 'date_of_birth', 'gender', 'state_of_origin', 'town', 'country',
      'occupation', 'year_joined'
      ]

      service.exportToExcel = function(members) {
        var q = $q.defer();
        var date = new Date();
        var defaultFilename = "staugustine_data_export_"+ date.getFullYear() + "_" + (date.getMonth() + 1) + "_" + date.getDate()+".xls";
        dialog.showSaveDialog({
          defaultPath: defaultFilename
        }, function(filename){
          if (!filename) {
            q.reject({
              error: "Did not get the file name"
            });
          } else {
            try {
              var xls = json2xls(members, {
                fields: fieldNames
              });
              fs.writeFile(filename, xls, 'binary', function(err){
                if (err){
                  q.reject({
                    error: err.message
                  })
                } else {
                  q.resolve({
                    messge: "saved"
                  })
                }
              });
            } catch (e) {
              q.reject({
                error: e.message
              })
            }
          }
        })

        return q.promise;
      };

      return service;
    }])
    .factory('myAppCache', ['CacheFactory', function (CacheFactory) {
      var statsCache = CacheFactory.get('statsCache');

      if (!statsCache) statsCache = CacheFactory('statsCache', {
        maxAge: 60 * 60 * 1000,
        deleteOnExpire: 'aggressive',
        storageMode: 'localStorage'
      });

      return {
        statsCache: statsCache
      }
    }])
    .provider('myAppSql', [function () {

      var knex;

      this.init = function (opt) {
        if (!opt.filename) {
          throw new Error("Path for SQLite not provided")
        }

        knex = require('knex')({
          client: 'sqlite3',
          connection: {
            filename: opt.filename
          },
          useNullAsDefault: true
        });

        createSchema();
      };

      function createSchema() {
        if (!knex) {
          throw new Error("myAppSql has not been initialised");
        }

        knex.schema.hasTable('mass_intentions')
          .then(function(exists){
            if (exists) {
              return knex.schema.table('mass_intentions', function(table){
                table.string("intention_type");
              });
            } else {
              return knex.schema.createTable('mass_intentions', function (table) {
                table.increments();
                table.text("intention");
                table.string("requested_by");
                table.date("starts_at");
                table.date("ends_at");
                table.dateTime("booked_on");
                table.integer("mass_pref");
                table.string("intention_type");
              })
            }
          })
          .then(function (res) {
          })
          .catch(function (err) {
          });

        knex.schema.createTableIfNotExists('thanksgivings', function (table) {
          table.increments();
          table.text("intention");
          table.string("requested_by");
          table.date("thanksgiving_date");
          table.integer("mass_pref");
          table.dateTime("booked_on")
        })
          .then(function (res) {
          })
          .catch(function (err) {
          });
      }

      this.$get = [function () {
        return knex;
      }]
    }])
    .service('myQ', ['$q', function ($q) {
      return {
        nodeify: function (fxn, args, context) {
          var q = $q.defer();
          var cb = function () {
            if (arguments[0]) {
              q.reject(arguments[0])
            } else {
              var res = Array.prototype.slice.apply(arguments, [1]);
              q.resolve(res);
            }
          };

          args.push(cb);

          fxn.apply(context, args);

          return q.promise;

        }
      }
    }])
})();
