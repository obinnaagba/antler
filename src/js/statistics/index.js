/**
 * Created by teliov on 3/26/17.
 */
(function () {
  angular.module('app.statistics', ['app.commons', 'chart.js'])
    .controller('StatisticsCtrl', ['$scope', '$state', '$q', 'myAppDb', 'myAppPopup', 'myAppProgress','myAppCache', 'AGE_GROUPS', 'STATES', 'SOCIETIES',
      function ($scope, $state, $q, myAppDb, myAppPopup, myAppProgress, myAppCache, AGE_GROUPS, STATES, SOCIETIES) {

        var db = myAppDb.getPouch();

        $scope.config = {
          legend: {
            display: true,
            position: 'top'
          }
        };

        function getCountStats (){
          var q = $q.defer();

          var stats = {
            members_total: function(){
              return db.find({
                selector :{
                  _id: {
                    $gt: null,
                    $regex: /^[0-9]/
                  }
                }
              })
                .then(function(res){
                  return res.docs.length;
                });
            },
            families_total: function(){
              return db.find({
                selector: {
                  _id: {
                    $gt: null,
                    $regex: /^family_/
                  }
                }

              })
                .then(function(res){
                  return res.docs.length;
                });
            },
            male_total: function(){
              return db.query('my_gender_index/by_gender', {
                key: "male"
              })
                .then(function(res){
                  return res.rows.length;
                });
            },
            female_total: function(){
              return db.query('my_gender_index/by_gender', {
                key: "female"
              })
                .then(function(res){
                  return res.rows.length;
                });
            },
            baptised_total: function() {
              return db.query('my_sacrament_index/by_sacraments', {
                key: "baptism"
              })
                .then(function(res){
                  return res.rows.length;
                });
            },
            communicant_total: function(){
              return db.query('my_sacrament_index/by_sacraments', {
                key: "communion"
              })
                .then(function(res){
                  return res.rows.length;
                });
            },
            confirmed_total: function(){
              return db.query('my_sacrament_index/by_sacraments', {
                key: "confirmation"
              })
                .then(function(res){
                  return res.rows.length;
                });
            },
            married_total: function(){
              return db.query('my_sacrament_index/by_sacraments', {
                key: "marriage"
              })
                .then(function(res){
                  return res.rows.length;
                });
            }
          };

          var promisesObj = {};
          var statsObjKeys = Object.keys(stats);
          statsObjKeys.forEach(function(key){
            promisesObj[key] = stats[key]();
          });

          $q.all(promisesObj)
            .then(function(results){
              q.resolve(results);
            })
            .catch(function(err){
              q.reject(err);
            });

          return q.promise;
        }

        function getGraphStats(){
          var q = $q.defer();

          var stats = {
            age_stats: function(){
              var q = $q.defer();
              var data = {};
              var label = {};
              _.each(AGE_GROUPS, function(val, key){
                data[key] = db.query('my_age_index/by_age', {
                  key: parseInt(key, 10)
                }).then(function(res){
                  return res.rows.length
                });
                label[key] = val.name
              });

              $q.all(data)
                .then(function(res){
                  q.resolve({
                    data: res,
                    label: label
                  });
                })
                .catch(function(err){
                  q.reject(err);
                });

              return q.promise;
            },
            states_stats: function(){
              var q = $q.defer();
              var data = {};
              var label = {};
              _.each(STATES, function(val, key){
                data[val.smallcase] = db.query('my_state_index/by_state', {
                  key: val.smallcase
                }).then(function(res){
                  return res.rows.length
                });
                label[val.smallcase] = val.ucfirst
              });

              $q.all(data)
                .then(function(res){
                  q.resolve({
                    data: res,
                    label: label
                  });
                })
                .catch(function(err){
                  q.reject(err);
                });

              return q.promise;
            },
            org_stats: function(){
              var q = $q.defer();
              var data = {};
              var label = {};
              _.each(SOCIETIES, function(val, key){
                data[val.abrev] = db.query('my_organization_index/by_organization', {
                  key: val.abrev
                }).then(function(res){
                  return res.rows.length
                });
                label[val.abrev] = val.name
              });

              $q.all(data)
                .then(function(res){
                  q.resolve({
                    data: res,
                    label: label
                  });
                })
                .catch(function(err){
                  q.reject(err);
                });

              return q.promise;
            }
          };

          var promisesObj = {};
          var statsObjKeys = Object.keys(stats);
          statsObjKeys.forEach(function(key){
            promisesObj[key] = stats[key]();
          });

          $q.all(promisesObj)
            .then(function(results){
              q.resolve(results);
            })
            .catch(function(err){
              q.reject(err);
            });

          return q.promise;
        }

        var drawStats = function(res){
          var countStats = res.count_stats;
          _.each(countStats, function(val, key){
            $scope[key] = val;
          });

          var ageStats = res.graph_stats.age_stats;
          var ageData = [];
          var ageLabels = [];
          var total = 0;
          _.each(ageStats.data, function(val, key){
            ageData.push(val);
            total += val;
            ageLabels.push(ageStats.label[key]);
          });
          var unknown = parseInt($scope.members_count, 10) - total;
          ageData.push(unknown);
          ageLabels.push("Unknown");

          $scope.age_data = ageData;
          $scope.age_labels = ageLabels;

          var statesStats = res.graph_stats.states_stats;
          var statesData = [];
          var statesLabel = [];
          _.each(statesStats.data, function(val,key){
            statesData.push(val);
            statesLabel.push(statesStats.label[key]);
          });
          $scope.states_data = statesData;
          $scope.states_labels = statesLabel;

          var orgStats = res.graph_stats.org_stats;
          var orgData = [];
          var orgLabel = [];
          _.each(orgStats.data, function(val,key){
            orgData.push(val);
            orgLabel.push(orgStats.label[key]);
          });

          $scope.org_data = orgData;
          $scope.org_labels = orgLabel;
        };

        var fetchStats = function(){
          var promises = {
            count_stats: getCountStats(),
            graph_stats: getGraphStats()
          };
          myAppProgress.start();
          $q.all(promises)
            .then(function(res){
              myAppProgress.stop();
              myAppCache.statsCache.put('stats', res);
              drawStats(res);
            })
            .catch(function(err){
              console.log(err);
              myAppProgress.stop();
              myAppPopup.alert({
                title: "Oops!",
                body: "Unable to load statistics"
              });
              $state.go("app.members", {}, {reload: true});
            });
        };

        var loadView = function(){
          var cache = myAppCache.statsCache;
          var stats = cache.get('stats');

          if (!stats){
            fetchStats();
          }else{
            drawStats(stats);
          }
        };

        $scope.refreshStats = function(){
          fetchStats();
        };

        loadView();
      }])
})();
