/**
 * Created by teliov on 12/28/16.
 */
(function(){
  angular.module('app.members', ['app.commons', 'ui.bootstrap.dropdown', 'ngMessages'])
    .controller('MembersCtrl', ['$scope','$state','SOCIETIES', 'AGE_GROUPS', 'SACRAMENTS','myAppDb','myAppPopup','myAppProgress','myAppExcelExport',
      function($scope, $state, societies, ageGroups, sacraments, myAppDb, myAppPopup, myAppProgress, myAppExcelExport){
      $scope.members = [];
      $scope.searcField = "";
      $scope.searchType = "1";
      $scope.organizations = societies;
      $scope.sacraments = sacraments;
      $scope.age_groups = ageGroups;

      $scope.doSearch = function(){
        var searchFn;
        var searchCb = function(res){
          var members = [];
          res.rows.forEach(function(row){
            var item = row.doc;
            if (typeof(item.firstname) == "undefined" || typeof(item.lastname) == "undefined") {
              var fullname = item.fullname;
              if (isString(fullname) && fullname.length > 0) {
                var parts = fullname.split(/\s+/);
                item.firstname = parts[0];
                item.lastname = parts.splice(1).join(" ");
              } else {
                item.firstname= null;
                item.lastname = null;
              }
            }
            members.push(item)
          });
          $scope.members = members;
        };
        switch ($scope.searchType){
          case "1":
            searchFn = myAppDb.getPouch()
              .search({
                query: $scope.searchField,
                fields: ['fullname'],
                include_docs: true
              });
            break;
          case "2":
            var orgId = $scope.searchOrgFilterId;
            var orgAbbrev = societies[orgId].abrev;
            searchFn = myAppDb.getPouch()
              .query('my_organization_index/by_organization', {
                key: orgAbbrev,
                include_docs: true
            });
            break;
          case "3":
            var sacId = $scope.searchSacFilterId;
            var sacName = sacraments[sacId].name;
            searchFn = myAppDb.getPouch()
              .query('my_sacrament_index/by_sacraments', {
                key: sacName,
                include_docs: true
              });
            break;
          case "4":
            var ageGroupId = $scope.searchAgeFilterId;
            searchFn =  myAppDb.getPouch()
              .query('my_age_index/by_age', {
                key: parseInt(ageGroupId, 10),
                include_docs: true
              });
            break;
          case "5":
            searchFn = myAppDb.getPouch()
              .search({
                query: $scope.searchField,
                fields: ['occupation'],
                include_docs: true
              });
            break;
          default:
            loadView();
            break
        }

        if (searchCb && searchFn){
          myAppProgress.start();
          searchFn.then(function(res){
            myAppProgress.stop();
            searchCb(res);
          })
          .catch(function(err){
            console.log(err);
            myAppProgress.stop();
            myAppPopup.alert({
              title: "Error",
              body: "Unable to execute search"
            });
          });
        }
      };

      $scope.sendSMS = function(){
        if ($scope.members.length == 0){
          myAppPopup.alert({
            title: "Error",
            body: "Unable to send sms to empty members list"
          });
          return;
        }
        var phoneNumbers = new Set();
        $scope.members.forEach(function(obj){
          if (obj.phone_number){
            phoneNumbers.add(obj.phone_number);
          }else if(obj.phone_number_2){
            phoneNumbers.add(obj.phone_number_2);
          }
        });

        phoneNumbers = Array.from(phoneNumbers);
        $state.go('app.sms',{recipients: phoneNumbers});
      };

      $scope.exportExcel = function(){
        if ($scope.members.length == 0){
          myAppPopup.alert({
            title: "Error",
            body: "Unable to export an members list"
          });
          return;
        }

        myAppProgress.start();
        myAppExcelExport.exportToExcel($scope.members)
          .then(function(){
            myAppPopup.open({
              title: "Success",
              body: "Export was successful"
            })
          })
          .catch(function(error){
            console.log(error);
            myAppPopup.alert({
              title: "Error",
              body: "Unable to fetch members"
            });
          })
          .finally(function(){
            myAppProgress.stop();
          })
      }

      $scope.clearSearch = function(){
        loadView();
      };

      function loadView (){
        myAppProgress.start();
        myAppDb.getPouch()
        .find({
          selector: {
            _id: {
              "$gt": null,
              "$regex": "^[0-9]"
            }
          }
        })
        .then(function(res){
          $scope.members = [];
          for (var idx =0; idx < res.docs.length; idx ++) {
            var item = res.docs[idx];
            if (typeof(item.firstname) === "undefined" || typeof(item.lastname) === "undefined") {
              if (isString(item.fullname) && item.fullname.length > 0) {
                var parts = item.fullname.split(/\s+/);
                item.firstname = parts[0];
                item.lastname = parts.splice(1).join(" ");
              } else {
                item.firstname = null;
                item.lastname = null;
              }
            }
            $scope.members.push(item);
          }
          myAppProgress.stop();
        })
        .catch(function(error){
          myAppProgress.stop();
          myAppPopup.alert({
            title: "Error",
            body: "Unable to fetch members"
          });
        })
      }

      $scope.deleteMember = function(member){
        myAppPopup.confirm({
          title: "Are you sure?",
          body: "Do you really want to delete this member, this action cannot be undone!",
          callback: function(ok){
            if (!ok) return;
            var db = myAppDb.getPouch();
            member = angular.extend(member, {
              _deleted: true
            });
            db.put(member)
              .then(function(){
                loadView();
                myAppPopup.open({
                  title: "Success",
                  body: "Member was successfully deleted"
                })
              })
              .catch(function(err){
                myAppPopup.alert({
                  title: "Oops!",
                  body: "Something went wrong while deleting this record"
                });
              })
          }
        })
      };

      // kick things off
      loadView();
    }])
    .controller('MemberRegCtrl', ['$scope','$state','SACRAMENTS','AGE_GROUPS', 'STATES', 'SOCIETIES', 'MONTHS','prepForReg','myAppDb','myAppPopup','myAppProgress',
      'myAppSms',
      function($scope, $state, sacraments, ageGroups, states, societies, months, prepForReg, myAppDb, myAppPopup, myAppProgress, myAppSms){

      $scope.title = "New Member Registration";
      $scope.member = {country: "Nigeria"};

      $scope.months = months;
      $scope.days = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
      $scope.age_groups = ageGroups;
      $scope.states = states;

      $scope.member.organizations = {};
      _.each(societies, function(val){
        $scope.member.organizations[val.id] = {abrev: val.abrev, selected: false}
      });

      $scope.member.sacraments = {};
      _.each(sacraments, function(val){
        $scope.member.sacraments[val.name] = {
          status: false,
          id: val.id,
          parish_of_reception: null,
          town_of_reception: null,
          year_of_reception:null,
          month_of_reception: null,
          day_of_reception: null
        }
      });

      $scope.register = function(regForm){
        if (regForm.$invalid) {
          return
        }
        var obj = prepForReg.prep($scope.member);

        var promiseLocator = null;
        myAppProgress.start();
        myAppDb.getPouch().put(obj)
          .then(function(doc){
            promiseLocator = "reg_complete"
            myAppPopup.open({
              title: "Success",
              body: "Registration Successful"
            });
            return doc;
          })
          .then(function(doc){
            promiseLocator = "sending sms";
            var phoneNumber = doc.phone_number;
            if (!phoneNumber){
              return;
            }
            return myAppSms.sendMessage([phoneNumber], "You have been successfully registered in the St Augustine Database");
          })
          .then(function(){
            $state.go($state.current, {}, {reload: true});
          })
          .catch(function(error){
            console.log(error);
            if (promiseLocator == "reg_complete") {
              $state.go($state.current, {}, {reload: true});
            } else {
              myAppPopup.alert({
              title: "Error",
              body: "Registration was not successful"
            });
            }
          })
          .finally(function(){
            myAppProgress.stop();
          })
      }
    }])
    .controller('MemberRegEditCtrl', ['$scope','$state','SACRAMENTS','AGE_GROUPS', 'STATES', 'SOCIETIES', 'MONTHS',
      'prepForReg','myAppDb','myAppPopup','myAppProgress','$stateParams', function($scope, $state, sacraments, ageGroups
        , states, societies, months, prepForReg, myAppDb, myAppPopup, myAppProgress, $stateParams){
      $scope.title = "Edit Member Registration";
      myAppProgress.reset();

      $scope.months = months;
      $scope.days = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
      $scope.age_groups = ageGroups;
      $scope.states = states;

      var initiateScopeMember = function(res){
        var member = res;
        var tempSacraments = {};
        _.each(sacraments, function(val){
          var sac = _.find(res.sacraments, function(s){return s.name.toLowerCase() == val.name.toLowerCase()});

          tempSacraments[val.name] = {
            status: sac ? true: false,
            id: val.id,
            parish_of_reception: sac ? sac.parish_of_reception : null,
            town_of_reception: sac ? sac.town_of_reception : null,
            year_of_reception:sac ? sac.year_of_reception : null,
            month_of_reception: sac ? sac.month_of_reception : null,
            day_of_reception: sac ? sac.day_of_reception : null
          }

        });
        member.sacraments = tempSacraments;

        var tempSocieties = {};
        _.each(societies, function(val){
          var org = _.find(res.organizations, function(o){return o.id == val.id});

          tempSocieties[val.id] = {abrev: val.abrev, selected: org ? true: false}

        });

        member.organizations = tempSocieties;

        var dobParts = member.date_of_birth ? member.date_of_birth : "";
        var dobParts = dobParts.split("-");
        if (!dobParts || dobParts.length !== 3) {
          member.birth_month = null;
          member.birth_day = null;
        }else {
          member.birth_month = dobParts[1].toString();
          member.birth_day = dobParts[2].toString();
        }

        delete  member.date_of_birth;

        if (member.age_group && typeof member.age_group == "object"){
          member.age_group = member.age_group.id.toString();
        }else {
          member.age_group = null;
        }

        if (typeof(member.firstname) === "undefined" || typeof(member.lastname) === "undefined") {
          var fullname = member.fullname;
          if (isString(fullname) && fullname.length > 0) {
            var parts = fullname.split(/\s+/);
            member.firstname = parts[0];
            member.lastname = parts.splice(1).join(" ");
          } else {
            member.firstname = null;
            member.lastname = null;
          }
        }

        $scope.member = member;
      };

      $scope.register = function(regForm){
        if (regForm.$invalid) return;

        myAppProgress.reset();
        myAppProgress.start();

        var obj = prepForReg.prep($scope.member);
        myAppDb.getPouch().put(obj)
          .then(function(doc){
            myAppPopup.open({
              title: "Success",
              body: "Edit was successful"
            });
            myAppProgress.complete();
            $state.go($state.current, {}, {reload: true})
          })
          .catch(function(error){
            console.log(error);
            myAppProgress.complete();
            myAppPopup.alert({
              title: "Error",
              body: "Edit was not successful"
            });
          })
      };

      myAppProgress.start();
      myAppDb.getPouch().get($stateParams.memberId)
        .then(function(res){
          initiateScopeMember(res);
          myAppProgress.complete();
        })
        .catch(function(err){
          console.log(err);
          myAppProgress.complete();
          myAppPopup.alert({
            title: "Error",
            message: "Unable to fetch member details"
          });
          $state.go("app.members", {}, {reload: true})
        });

    }])
    .controller('MemberCtrl', ['$scope', '$state','myAppDb','myAppPopup', 'myAppProgress','$stateParams','SACRAMENTS'
      ,'SOCIETIES', function($scope, $state, myAppDb, myAppPopup, myAppProgress, $stateParams, sacraments, societies){

      $scope.title = "Member Detail";
      myAppProgress.reset();

      var initiateScopeMember = function(res){
        var member = res;
        var dobParts = member.date_of_birth.split("-");
        if (!dobParts || !dobParts.length == 3) {
          member.birth_month = null;
          member.birth_day = null;
        }else {
          member.birth_month = dobParts[1].toString();
          member.birth_day = dobParts[2].toString();
        }


        if (member.age_group && typeof member.age_group == "object"){
          member.age_group = member.age_group.id.toString();
        }else {
          member.age_group = null;
        }

        if (typeof(member.firstname) === "undefined" || typeof(member.lastname) === "undefined") {
          var fullname = member.fullname;
          if (isString(fullname) && fullname.length >0) {
            var parts = fullname.split(/\s+/);
            member.firstname = parts[0];
            member.lastname = parts.splice(1).join(" ");
          } else {
            member.lastname = null;
            member.firstname = null;
          }
        }

        $scope.member = member;
      };


      myAppProgress.start();
      myAppDb.getPouch().get($stateParams.memberId)
        .then(function(res){
          initiateScopeMember(res);
          myAppProgress.complete();
        })
        .catch(function(err){
          console.log(err);
          myAppProgress.complete();
          myAppPopup.alert({
            title: "Error",
            message: "Unable to fetch member details"
          });
          $state.go("app.members", {}, {reload: true})
        })

    }])
    .factory('prepForReg', ['SOCIETIES', 'myAppUtils', 'AGE_GROUPS', function(SOCIETIES, myAppUtils, AGE_GROUPS){

      return {
        prep: function(member) {
          var sacraments = [];
          var societies = [];
          var sacKeys = Object.keys(member.sacraments);
          var orgKeys = Object.keys(member.organizations);
          sacKeys.forEach(function(key){
            var val = member.sacraments[key];
            if (val.status) {
              sacraments.push(angular.extend(val, {name:key}))
            }
          });
          orgKeys.forEach(function(key){
            var val = member.organizations[key];
            if (val.selected) {
              societies.push(SOCIETIES[key]);
            }
          });

          var firstname = member.firstname ? member.firstname.trim().toLowerCase() : "";
          var lastname = member.lastname ?  member.lastname.trim().toLowerCase(): "";
          var fullname = firstname + " " + lastname;
          fullname = fullname.trim();

          var obj = {
            _id: member._id? member._id : new Date().toJSON() + myAppUtils.machineId.machineIdSync(),
            reg_no: member.reg_no,
            fullname: fullname,
            firstname: firstname,
            lastname: lastname,
            address: member.address ? member.address.toLowerCase() : "",
            phone_number: member.phone_number,
            phone_number_2: member.phone_number_2,
            email: member.email ? member.email.toLowerCase() : "",
            date_of_birth: member.birth_month && member.birth_day ? _.join(["0004", member.birth_month, member.birth_day], "-") : null,
            gender: member.gender ? member.gender.toLowerCase() : "",
            country: member.country ? member.country.toLowerCase() : "",
            state_of_origin: member.state_of_origin ? member.state_of_origin.toLowerCase() : "",
            town: member.town ? member.town.toLowerCase() : "",
            occupation: member.occupation ? member.occupation.toLowerCase() : "",
            year_joined: member.year_joined,
            blood_group: member.blood_group,
            age_group: AGE_GROUPS[member.age_group],
            sacraments: sacraments,
            organizations: societies,
            registered_at: member.registered_at ? member.registered_at: myAppUtils.moment().format('YYYY-MM-DD HH:mm:ss')
          };

          return angular.extend(member, obj);
        }
      }
    }]);
})();
