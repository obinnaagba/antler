/**
 * Created by teliov on 12/27/16.
 */

(function () {
  var bcrypt = require('bcryptjs');

  angular.module('app.login', ['app.commons'])
    .controller('LoginCtrller', ['$scope', '$state', 'myAppPopup', 'myAppDb', 'myAppProgress'
      , function ($scope, $state, myAppPopup, myAppDb, myAppProgress) {

        (function(){
          var element = angular.element('body');
          if (element.hasClass('nav-md')) element.removeClass('nav-md');
          if (!element.hasClass('login')) element.addClass('login');
        })();

        $scope.credentials = {};
        $scope.doLogin = function (form) {
          if (form.$invalid) return null;

          myAppProgress.start();
          myAppDb.getAdminPouch()
            .query('my_username_index/by_username', {
                key: $scope.credentials.username,
                include_docs: true
              })
            .then(function(res){
              if (res.rows.length < 1) {
                myAppPopup.alert({
                  title: "Error",
                  body: "Invalid Login Details"
                });
                return;
              };

              var user = null;
              for (var idx = 0; idx < res.rows.length; idx++) {
                var row = res.rows[idx];
                if (row.doc.username === $scope.credentials.username) {
                  user = row.doc;
                  break;
                }
              }

              if (!user) {
                myAppPopup.alert({
                  title: "Error",
                  body: "Invalid Login Details"
                });
                return;
              }

              if (!bcrypt.compareSync($scope.credentials.password, user.password)) {
                myAppPopup.alert({
                  title: "Error",
                  body: "Invalid Login Details"
                });
                return;
              }

              $state.go('app.members');
            })
            .catch(function(error){
              console.log(error);
              myAppPopup.alert({
                title: "Error",
                body: "Invalid Login Details"
              });
            })
            .finally(function(){
              myAppProgress.stop();
            })
        }
      }])
})();
