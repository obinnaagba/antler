/**
 * Created by teliov on 12/27/16.
 */
(function () {
  'use strict';

  angular.module('app', ['ui.router', 'app.login', 'app.members', 'pouchdb', 'app.families', 'app.sms', 'app.statistics', 'app.intentions', 'app.thanksgiving', 'app.home'])
    .run(['myAppDb', function (db) {
      var syncHandler = db.getSyncHandler();
      syncHandler
        .on('change', function (change) {
        })
        .on('paused', function (info) {
        })
        .on('active', function (info) {
        })
        .on('error', function (err) {
        })
        .on('denied', function (err) {
        });

      var userSyncHandler = db.getAdminSyncHandler();
      userSyncHandler
        .on('change', function (change) {
        })
        .on('paused', function (info) {
        })
        .on('active', function (info) {
        })
        .on('error', function (err) {
        })
        .on('denied', function (err) {
        });
    }])
    .config(['$stateProvider', '$urlRouterProvider', 'myAppDbProvider','myAppSqlProvider', 'tagsInputConfigProvider','calendarConfig',
      'ENVIRONMENT', 'POUCHDB_URL', 'POUCHDB_USER', 'POUCHDB_PASSWORD', 'POUCHDB_USER_URL'
      , function ($sP, $uRP, myAppDbP, myAppSqlP, ngTagsConfig, calendarConfig, env, pouchUrl, pouchUser, pouchPw, pouchUserUrl) {

        ngTagsConfig.setDefaults('tagsInput', {
          addOnPaste: true,
          replaceSpacesWithDashes: false
        });

        var localDbPath, app, localSqlPath, localUserDbPath;
        app = require('electron').remote.app;
        localDbPath = env == "production" ? app.getPath('userData') + "/staugustine" : app.getPath('userData') + "/staugustinetest";
        localUserDbPath = env == "production" ? app.getPath('userData') + "/staugustinedb_user" : app.getPath('userData') + "/staugustinetestdb_user";
        localSqlPath =  env == "production" ? app.getPath('userData') + "/staugustine.sqlite" : app.getPath('userData') + "staugustinetest.sqlite";

        var dbInitOpts = {
          localPath: localDbPath,
          remoteUrl: pouchUrl,
          userLocalPath: localUserDbPath,
          remoteUserUrl: pouchUserUrl
        };

        if (pouchUser && pouchPw) {
          dbInitOpts = angular.extend(dbInitOpts, {
            username: pouchUser,
            password: pouchPw
          })
        }
        myAppDbP.init(dbInitOpts);

        myAppSqlP.init({
          filename: localSqlPath
        });

        $sP
          .state('login', {
            url: '/login',
            controller: 'LoginCtrller',
            templateUrl: 'assets/templates/login.html'
          })
          .state('app', {
            abstract: true,
            url: '/app',
            templateUrl: 'assets/templates/base.html',
            controller: 'BaseCtrller'
          })
          .state('app.home', {
            url: '/home',
            views: {
              mainContent: {
                templateUrl: 'assets/templates/home.html',
                controller: 'HomeCtrl'
              }
            }
          })
          .state('app.birthdays_list', {
            url: '/birthdays_list',
            views: {
              mainContent: {
                templateUrl: 'assets/templates/birthdays-list.html',
                controller: 'BirthdaysListCtrl'
              }
            },
            params: {
              date: null
            }
          })
          .state('app.weddings_list', {
            url: '/weddings_list',
            views: {
              mainContent: {
                templateUrl: 'assets/templates/weddings-list.html',
                controller: 'WeddingsListCtrl'
              }
            },
            params: {
              date: null
            }
          })
          .state('app.members', {
            url: '/members',
            views: {
              mainContent: {
                templateUrl: 'assets/templates/members.html',
                controller: 'MembersCtrl'
              }
            }
          })
          .state('app.families', {
            url: '/families',
            views: {
              mainContent: {
                templateUrl: 'assets/templates/families.html',
                controller: 'FamiliesCtrl'
              }
            }
          })
          .state('app.member_registration', {
            url: '/member_reg',
            views: {
              mainContent: {
                templateUrl: 'assets/templates/member_registration.html',
                controller: 'MemberRegCtrl'
              }
            }
          })
          .state('app.family_registration', {
            url: '/family_reg',
            views: {
              mainContent: {
                templateUrl: 'assets/templates/family_registration.html',
                controller: 'FamilyRegCtrl'
              }
            }
          })
          .state('app.member', {
            url: '/member/:memberId',
            views: {
              mainContent: {
                templateUrl: 'assets/templates/member.html',
                controller: 'MemberCtrl'
              }
            }
          })
          .state('app.family', {
            url: '/family/:familyId',
            views: {
              mainContent: {
                templateUrl: 'assets/templates/family.html',
                controller: 'FamilyCtrl'
              }
            }
          })
          .state('app.member_edit', {
            url: '/member/:memberId/edit',
            views: {
              mainContent: {
                templateUrl: 'assets/templates/member_registration.html',
                controller: 'MemberRegEditCtrl'
              }
            }
          })
          .state('app.family_edit', {
            url: '/family/:familyId/edit',
            views: {
              mainContent: {
                templateUrl: 'assets/templates/family_registration.html',
                controller: 'FamilyRegEditCtrl'
              }
            }
          })
          .state('app.sms', {
            url: '/sms',
            views: {
              mainContent: {
                templateUrl: 'assets/templates/send_sms.html',
                controller: 'SmsCtrl'
              }
            },
            params: {
              message: null,
              recipients: null
            }
          })
          .state('app.statistics', {
            url: '/statistics',
            views: {
              mainContent: {
                templateUrl: 'assets/templates/statistics.html',
                controller: 'StatisticsCtrl'
              }
            }
          })
          .state('app.intentions', {
            url: '/intentions',
            views: {
              mainContent: {
                templateUrl: 'assets/templates/mass_intentions.html',
                controller: 'MassIntentionsCtrl'
              }
            }
          })
          .state('app.intentions_list', {
            url: '/intentions_list',
            views: {
              mainContent: {
                templateUrl: 'assets/templates/intention-list.html',
                controller: 'MassIntentionsListCtrl'
              }
            },
            params: {
              date: null
            }
          })
          .state('app.intentions_edit', {
            url: '/intentions/:intentionId/edit',
            views: {
              mainContent: {
                templateUrl: 'assets/templates/mass_intentions.html',
                controller: 'MassIntentionsEditCtrl'
              }
            }
          })
          .state('app.thanksgiving', {
            url: '/thanksgiving',
            views: {
              mainContent: {
                templateUrl: 'assets/templates/thanksgiving.html',
                controller: 'ThanksgivingCtrl'
              }
            }
          })
          .state('app.thanksgiving_list', {
            url: '/thanksgiving_list',
            views: {
              mainContent: {
                templateUrl: 'assets/templates/thanksgiving-list.html',
                controller: 'ThanksgivingListCtrl'
              }
            },
            params: {
              date: null
            }
          })
          .state('app.thanksgiving_edit', {
            url: '/thanksgivings/:thanksgivingId/edit',
            views: {
              mainContent: {
                templateUrl: 'assets/templates/thanksgiving.html',
                controller: 'ThanksgivingEditCtrl'
              }
            }
          });


        $uRP
          .otherwise(function ($injector) {
            var $state = $injector.get("$state");
            $state.go("login");
          })
      }])
})();
