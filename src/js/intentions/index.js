/**
 * Created by teliov on 3/28/17.
 */
(function () {
  angular.module('app.intentions', ['app.commons', 'ngMessages', 'ui.bootstrap', 'angularMoment'])
    .controller('MassIntentionsCtrl', ['$scope', '$state', '$q', 'myAppPopup', 'myAppProgress', 'myAppSql'
      , function ($scope, $state, $q, myAppPopup, myAppProgress, myAppSql) {

        $scope.title = "New Mass Intention";

        var initDate = moment().add(1, 'days').toDate();
        $scope.intention = {
          starts_at: initDate,
          ends_at: initDate,
          intention_type: "regular"
        };

        $scope.startsAtOptions = $scope.endsAtOptions = {
          initDate: initDate
        };


        $scope.saveIntention = function (form) {
          if (form.$invalid) {
            return null;
          }

          var dbObj = angular.extend($scope.intention);
          if (dbObj.starts_at > dbObj.ends_at){
            myAppPopup.alert({
              "title": "Oops!",
              "body": "End Date for intention cannot be before start date"
            });
            return;
          }

          dbObj.booked_on = moment().format("YYYY-MM-DD");
          dbObj.starts_at = moment(dbObj.starts_at).format("YYYY-MM-DD");
          dbObj.ends_at = moment(dbObj.ends_at).format("YYYY-MM-DD");
          myAppProgress.start();
          $q.when(myAppSql("mass_intentions")
            .insert(dbObj))
            .then(function (res) {
              myAppPopup.open({
                title: "Success",
                body: "Intention was saved successfully"
              });
              $state.go($state.current, {}, {reload: true});
            })
            .catch(function (err) {
              console.log(err);
              myAppPopup.alert({
                title: "Error",
                body: "Something went wrong trying to save this intention"
              });
            })
            .finally(function () {
              myAppProgress.stop();
            });
        }

      }])
    .controller('MassIntentionsListCtrl', ['$scope', '$state', '$stateParams','$q', 'myAppProgress', 'myAppPopup', 'myAppSql', 'myQ'
      , function ($scope, $state, $stateParams, $q, myAppProgress, myAppPopup, myAppSql, myQ) {
        var date = $stateParams.date ? moment($stateParams.date) : moment();

        $scope.title = date.format("dddd, MMMM Do YYYY");
        $scope.filter = "all";
        $scope.regular_intentions = [];
        $scope.rip_intentions = [];

        $scope.deleteIntention = function(intention){
          myAppPopup.confirm({
            title: "Do you really want to delete?",
            body: "This action cannot be undone!",
            callback: function(ok){
              if (ok){
                $q.when(
                  myAppSql("mass_intentions")
                    .where("id", intention.id)
                    .delete()
                )
                  .then(function(){
                    if (typeof intention.intention_type != "undefined" && intention.intention_type =="rip") {
                      $scope.rip_intentions = $scope.rip_intentions.filter(function(item){
                        return item.id !== intention.id;
                      });
                    } else {
                      $scope.regular_intentions = $scope.regular_intentions.filter(function(item){
                        return item.id !== intention.id;
                      });
                    }
                  })
                  .catch(function(err){
                    console.log(err);
                    $scope.alert({
                      title: "Oops!",
                      body: "Something went wrong deleting this mass intention"
                    });
                  })
              }
            }
          })
        };

        function getIntentions(massPref){
          var promise = myAppSql("mass_intentions")
            .where("starts_at", "<=", date.startOf('day').format("YYYY-MM-DD"))
            .andWhere("ends_at", ">=", date.startOf('day').format("YYYY-MM-DD"));

          if (massPref && (massPref == "1" || massPref == "2")){
            promise = promise
              .andWhere("mass_pref", massPref);
          }

          return $q.when(promise);
        }

        function assignIntentions(res){
          $scope.rip_intentions = [];
          $scope.regular_intentions = [];
          res.forEach(function(intention){
            if (typeof intention.intention_type != "undefined" && intention.intention_type == "rip") {
              $scope.rip_intentions.push(intention);
            } else {
              $scope.regular_intentions.push(intention);
            }
          });
        }

        function loadView(){
          myAppProgress.start();
          getIntentions()
            .then(function(res){
              assignIntentions(res);
            })
            .catch(function(err){
              console.log(err);
              myAppPopup.alert({
                title: "Oops!",
                body: "Unable to fetch the intentions for this day"
              })
            })
            .finally(function(){
              myAppProgress.stop();
            })
        }

        $scope.filterByMass = function(type){
          var promise;
          $scope.filter = type;
          switch (type){
            case 'first':
              promise = getIntentions("1");
              break;
            case 'second':
              promise = getIntentions("2");
              break;
            default:
              $scope.filter = "all";
              promise = getIntentions();
          }

          promise
            .then(function(res){
              assignIntentions(res);
            })
            .catch(function(err){
              console.log(err);
              myAppProgress.stop();
              myAppPopup.alert({
                title: "Oops!",
                body: "Unable to fetch the intentions for this day"
              })
            })
            .finally(function(){
              myAppProgress.stop();
            })
        };

        $scope.print = function(){
          var html = "<html>";
          var isSunday = date.day() == 0;
          var thisDate = date.format("dddd, MMMM Do YYYY");
          var headerArgs = [thisDate];
          var secondArg = "All Masses";
          if (isSunday && $scope.filter != "all"){
            secondArg = $scope.filter == "first" ? "First Mass" : "Second Mass"
          }
          headerArgs.push(secondArg);
          var heading = "<h2>Mass Intentions for %s: <b>%s</b></h2>";
          heading =  vsprintf(heading, headerArgs);
          html += heading;

          var idx;
          var intention;
          var line;

          for (idx=0; idx < $scope.regular_intentions.length; ++idx){
            intention = $scope.regular_intentions[idx];
            line = "<div>" +
                      "<p style='font-size: 22px;font-weight: 500'>%d.) %s</p>" +
                      "<p style='font-size: 20px;'>-- Requested By: %s</p>" +
                    "</div>";
            html += vsprintf(line, [idx+1, intention.intention, intention.requested_by])
          }

          html += "<h3 style='margin-top: 10px; margin-bottom: 10px'>Requiem Intentions</h3>";

          for (idx=0; idx < $scope.rip_intentions.length; ++idx){
            intention = $scope.rip_intentions[idx];
            line = "<div>" +
                    "<p style='font-size: 22px;font-weight: 500'>%d.) %s</p>" +
                    "<p style='font-size: 20px;'>-- Requested By: %s</p>" +
                  "</div>";
            html += vsprintf(line, [idx+1, intention.intention, intention.requested_by])
          }

          html += "</html>";

          var fs = require('fs');
          var info;

          myQ.nodeify(nodeTemp.open, ['staugustinedb'])
            .then(function(args){
              info = args[0];
              return myQ.nodeify(fs.write, [info.fd, html]);
            })
            .then(function(){
              return myQ.nodeify(fs.close, [info.fd]);
            })
            .then(function(){
              var ipcRenderer = require("electron").ipcRenderer;
              ipcRenderer.send('print-command', info.path);
            })
            .catch(function(err){
              console.log(err);
              myAppPopup.alert({
                title: "Oops!",
                body: "Something went wrong trying to print"
              })
            })

        };

        loadView();

      }])
    .controller('MassIntentionsEditCtrl', ['$scope', '$state', '$stateParams','$q', 'myAppSql', 'myAppProgress', 'myAppPopup'
      ,function ($scope, $state, $stateParams,$q, myAppSql, myAppProgress, myAppPopup) {

        $scope.title = "Edit Mass Intention";
        $scope.isEdit = true;
        $scope.startsAtOptions = $scope.endsAtOptions = {};

        var loadView = function(){
          var intentionId = $stateParams.intentionId;
          if (!intentionId){
            myAppPopup.alert({
              title: "Oops!",
              body: "Unable to fetch this intention"
            });
            $state.go("app.intentions", {}, {reload: true});
          }

          $q.when(
            myAppSql("mass_intentions")
            .where("id", intentionId)
          )
            .then(function(res){
              var intention = res[0];

              $scope.intention = angular.extend(intention, {
                starts_at: moment(intention.starts_at).toDate(),
                ends_at: moment(intention.ends_at).toDate()
              });

              $scope.startsAtOptions = $scope.endsAtOptions = {
                initDate: new Date()
              };
            })
        };

        $scope.deleteIntention = function(){
          myAppPopup.confirm({
            title: "Do you really want to delete?",
            body: "This action cannot be undone!",
            callback: function(ok){
              if (ok){
                deleteIntention();
              }
            }
          })
        };

        function deleteIntention(){
          $q.when(
            myAppSql("mass_intentions")
            .where("id", $scope.intention.id)
            .delete()
          )
            .then(function(){
              myAppPopup.open({
                title: "Success",
                body: "Mass Intention has been deleted"
              });
              $state.go("app.intentions", {}, {reload: true})
            })
            .catch(function(err){
              console.log(err);
              myAppPopup.alert({
                title: "Oops!",
                body: "Unable to delete mass intention"
              })
            })
        }

        $scope.saveIntention = function(form){
          if (form.$invalid){
            return null
          }

          var dbObj = angular.extend($scope.intention);
          if (dbObj.starts_at > dbObj.ends_at){
            myAppPopup.alert({
              "title": "Oops!",
              "body": "End Date for intention cannot be before start date"
            });
            return;
          }

          dbObj.starts_at = moment(dbObj.starts_at).format("YYYY-MM-DD");
          dbObj.ends_at = moment(dbObj.ends_at).format("YYYY-MM-DD");

          var id = dbObj.id;
          delete dbObj.id;

          myAppProgress.start();
          $q.when(myAppSql("mass_intentions")
            .where("id", id)
            .update(dbObj))
            .then(function(){
              myAppPopup.open({
                title: "Success",
                body: "Mass intention was successfully updated"
              })
            })
            .catch(function(err){
              myAppPopup.alert({
                title: "Oops!",
                body: "There was an error updating this mass intention"
              });
              console.log(err);
            })
            .finally(function(){
              myAppProgress.stop();
            })
        };

        loadView();
    }])
})();
