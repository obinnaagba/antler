/**
 * Created by teliov on 3/28/17.
 */
(function () {
  angular.module('app.thanksgiving', ['app.commons', 'ngMessages', 'ui.bootstrap', 'angularMoment'])
    .controller('ThanksgivingCtrl', ['$scope', '$state', '$q', 'myAppPopup', 'myAppProgress', 'myAppSql'
      , function ($scope, $state, $q, myAppPopup, myAppProgress, myAppSql) {

        $scope.title = "New Thanksgiving";
        var initDate = moment().add(1, 'days').toDate();

        $scope.intention = {
          thanksgiving_date: initDate
        };

        $scope.dateOptions = {
          initDate: initDate
        };

        $scope.saveThanksgiving = function (form) {
          if (form.$invalid) {
            return null;
          }

          // check that if it's a sunday and it's first mass, there aren't already 3
          // thanksgivings for that day

          var q = $q.defer();
          var date = moment($scope.intention.thanksgiving_date);
          if (date.day() == 0 && $scope.intention.mass_pref == "1") {
            myAppSql("thanksgivings")
              .where({
                thanksgiving_date: date.format("YYYY-MM-DD")
              })
              .count("id as count")
              .then(function (res) {
                console.log(res);
                if (res.length != 1) {
                  q.reject("error fetching count");
                }

                if (res[0].count < 3) {
                  q.resolve({
                    status: true,
                    message: "okay"
                  });
                } else {
                  q.resolve({
                    status: false,
                    message: "There already 3 thanksgivings on for this mass"
                  });
                }
              })
              .catch(function (err) {
                console.log(err);
                q.reject(err);
              });
          } else {
            q.resolve({
              status: true,
              message: "okay"
            });
          }

          q.promise
            .then(function (res) {
              if (!res.status) {
                myAppPopup.alert({
                  title: "Oops!",
                  body: res.message
                });
                return;
              }

              console.log($scope.intention);
              var dbObj = angular.extend($scope.intention);
              dbObj.thanksgiving_date = moment(dbObj.thanksgiving_date).format("YYYY-MM-DD");
              dbObj.booked_on = moment().format("YYYY-MM-DD");

              myAppProgress.start();
              return myAppSql("thanksgivings")
                .insert(dbObj)
                .then(function (res) {
                  myAppProgress.stop();
                  myAppPopup.open({
                    title: "Success",
                    body: "Thanksgiving was saved successfully"
                  });
                  $state.go($state.current, {}, {reload: true});
                })
            })
            .catch(function (err) {
              myAppProgress.stop();
              myAppPopup.alert({
                title: "Oops!",
                body: "Something went wrong trying to save this thanksgiving"
              })
            })
        }
      }])
    .controller("ThanksgivingListCtrl", ['$scope', '$state', '$stateParams', '$q', 'myAppSql', 'myAppProgress', 'myAppPopup','myQ'
      , function ($scope, $state, $stateParams, $q, myAppSql, myAppProgress, myAppPopup, myQ) {

        var date = $stateParams.date ? moment($stateParams.date) : moment();
        $scope.title = date.format("dddd, MMMM Do YYYY");
        $scope.fiter = "all";
        $scope.intentions = [];

        $scope.print = function(){
          var html = "<html>";
          var isSunday = date.day() == 0;
          var thisDate = date.format("dddd, MMMM Do YYYY");
          var headerArgs = [thisDate];
          var secondArg = "All Masses";
          if (isSunday && $scope.filter != "all"){
            secondArg = $scope.filter == "first" ? "First Mass" : "Second Mass"
          }
          headerArgs.push(secondArg);
          var heading = "<h2>Thanksgiving for %s: <b>%s</b></h2>";
          heading =  vsprintf(heading, headerArgs);
          html += heading;

          for (var idx=0; idx < $scope.intentions.length; ++idx){
            var intention = $scope.intentions[idx];
            var line = "<div><p>%d.) %s</p><p>-- Requested By: %s</p></div>";
            html += vsprintf(line, [idx+1, intention.intention, intention.requested_by])
          }

          html += "</html>";

          var fs = require('fs');
          var info;

          myQ.nodeify(nodeTemp.open, ['staugustinedb'])
            .then(function(args){
              info = args[0];
              return myQ.nodeify(fs.write, [info.fd, html]);
            })
            .then(function(){
              return myQ.nodeify(fs.close, [info.fd]);
            })
            .then(function(){
              var ipcRenderer = require("electron").ipcRenderer;
              ipcRenderer.send('print-command', info.path);
            })
            .catch(function(err){
              console.log(err);
              myAppPopup.alert({
                title: "Oops!",
                body: "Something went wrong trying to print"
              })
            })

        };

        $scope.deleteIntention = function (intention) {
          myAppPopup.confirm({
            title: "Do you really want to delete?",
            body: "This action cannot be undone!",
            callback: function (ok) {
              if (ok) {
                $q.when(
                  myAppSql("thanksgivings")
                    .where("id", intention.id)
                    .delete()
                )
                  .then(function () {
                    $scope.intentions = $scope.intentions.filter(function (item) {
                      return item.id !== intention.id;
                    })
                  })
                  .catch(function (err) {
                    console.log(err);
                    $scope.alert({
                      title: "Oops!",
                      body: "Something went wrong deleting this thanksgiving"
                    });
                  })
              }
            }
          })
        };

        function getIntentions(massPref) {
          var promise = myAppSql("thanksgivings")
            .whereBetween("thanksgiving_date", [date.startOf('day').format("YYYY-MM-DD"), date.endOf('day').format("YYYY-MM-DD")]);

          if (massPref && (massPref == "1" || massPref == "2")) {
            promise = promise
              .andWhere("mass_pref", massPref);
          }

          return $q.when(promise);
        }

        function loadView() {
          myAppProgress.start();
          getIntentions()
            .then(function (res) {
              $scope.intentions = res;
            })
            .catch(function (err) {
              console.log(err);
              myAppPopup.alert({
                title: "Oops!",
                body: "Unable to fetch the intentions for this day"
              })
            })
            .finally(function () {
              myAppProgress.stop();
            });
        }

        $scope.filterByMass = function (type) {
          var promise;
          $scope.filter = type;
          switch (type) {
            case 'first':
              promise = getIntentions("1");
              break;
            case 'second':
              promise = getIntentions("2");
              break;
            default:
              $scope.filter = "all";
              promise = getIntentions();
          }

          promise
            .then(function (res) {
              $scope.intentions = res;
            })
            .catch(function (err) {
              console.log(err);
              myAppProgress.stop();
              myAppPopup.alert({
                title: "Oops!",
                body: "Unable to fetch the intentions for this day"
              })
            })
            .finally(function () {
              myAppProgress.stop();
            })
        };

        loadView();

      }])
    .controller("ThanksgivingEditCtrl", ['$scope', '$state', '$stateParams', '$q', 'myAppSql', 'myAppProgress', 'myAppPopup'
      , function ($scope, $state, $stateParams, $q, myAppSql, myAppProgress, myAppPopup) {

        $scope.title = "Edit Thanksgiving";

        $scope.isEdit = true;

        var loadView = function () {
          var intentionId = $stateParams.thanksgivingId;
          if (!intentionId) {
            myAppPopup.alert({
              title: "Oops!",
              body: "Unable to fetch this thanksgiving"
            });
            $state.go("app.thanksgiving", {}, {reload: true});
          }

          $q.when(
            myAppSql("thanksgivings")
              .where("id", intentionId)
          )
            .then(function (res) {
              var intention = res[0];

              $scope.intention = angular.extend(intention, {
                starts_at: moment(intention.starts_at).toDate(),
                ends_at: moment(intention.ends_at).toDate()
              });

              $scope.dateOptions = $scope.endsAtOptions = {
                initDate: new Date()
              };
            })
        };

        $scope.deleteIntention = function () {
          myAppPopup.confirm({
            title: "Do you really want to delete?",
            body: "This action cannot be undone!",
            callback: function (ok) {
              if (ok) {
                deleteIntention();
              }
            }
          })
        };

        function deleteIntention() {
          $q.when(
            myAppSql("thanksgivings")
              .where("id", $scope.intention.id)
              .delete()
          )
            .then(function () {
              myAppPopup.open({
                title: "Success",
                body: "Thanksgiving has been deleted"
              });
              $state.go("app.thanksgiving", {}, {reload: true})
            })
            .catch(function (err) {
              console.log(err);
              myAppPopup.alert({
                title: "Oops!",
                body: "Unable to delete thanksgiving"
              })
            })
        }

        $scope.saveThanksgiving = function (form) {
          if (form.$invalid) {
            return null;
          }

          // check that if it's a sunday and it's first mass, there aren't already 3
          // thanksgivings for that day

          var dbObj = angular.extend({}, $scope.intention);
          var q = $q.defer();
          var date = moment(dbObj.thanksgiving_date);
          if (date.day() == 0 && dbObj.mass_pref == "1") {
            myAppSql("thanksgivings")
              .where({
                thanksgiving_date: date.format("YYYY-MM-DD")
              })
              .andWhereNot("id", dbObj.id)
              .count("id as count")
              .then(function (res) {
                console.log(res);
                if (res.length != 1) {
                  q.reject("error fetching count");
                }

                if (res[0].count < 3) {
                  q.resolve({
                    status: true,
                    message: "okay"
                  });
                } else {
                  q.resolve({
                    status: false,
                    message: "There already 3 thanksgivings on for this mass"
                  });
                }
              })
              .catch(function (err) {
                console.log(err);
                q.reject(err);
              });
          } else {
            q.resolve({
              status: true,
              message: "okay"
            });
          }

          q.promise
            .then(function (res) {
              console.log(res)
              if (!res.status) {
                myAppPopup.alert({
                  title: "Oops!",
                  body: res.message
                });
                return;
              }


              dbObj.thanksgiving_date = moment(dbObj.thanksgiving_date).format("YYYY-MM-DD");

              myAppProgress.start();
              var thanksgivingId = dbObj.id;
              delete dbObj.id;
              return myAppSql("thanksgivings")
                .where("id", thanksgivingId)
                .insert(dbObj)
                .then(function (res) {
                  myAppProgress.stop();
                  myAppPopup.open({
                    title: "Success",
                    body: "Thanksgiving was updated successfully"
                  });
                  $state.go($state.current, {}, {reload: true});
                })
            })
            .catch(function (err) {
              myAppProgress.stop();
              myAppPopup.alert({
                title: "Oops!",
                body: "Something went wrong trying to update this thanksgiving"
              })
            })
        }

        loadView();
      }])
})();
