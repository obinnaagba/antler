/**
 * Created by teliov on 3/16/17.
 */
(function () {
  angular.module('app.sms', ['app.commons', 'ngTagsInput', 'ngMessages'])
    .controller('SmsCtrl', ["$scope", "$stateParams", "myAppSms", "myAppProgress", "myAppPopup", function ($scope, $stateParams, myAppSms, myAppProgress, myAppPopup) {

      $scope.sms = {};
      if ($stateParams.message) $scope.sms.message = $stateParams.message;

      if ($stateParams.recipients) {
        $scope.sms.recipients = $stateParams.recipients
      }

      $scope.sendSMS = function(smsForm){
        if (smsForm.$invalid) return null;

        if (!window.navigator.onLine) {
          // computer is not online
          myAppPopup.alert({
            title: "Oops!",
            body: "Sorry, cannot perform this action when you are offline. Please connect to the internet and try again"
          });
          return null;
        }

        myAppProgress.start();
        var recipients = $scope.sms.recipients.split(/,|\s+/);

        myAppSms.sendSms($scope.sms.message, recipients)
          .then(function(res){
            $scope.sms = {};
            var numOfSuccessful = 0;
            var numOfFailed = 0;
            console.log(res);
            res.successes.forEach(function(success){
              if (!success) {
                return;
              }
              if (angular.isDefined(success.recipients)){
                numOfSuccessful += success.recipients;
              }

              if (angular.isDefined(success.failed)){
                numOfFailed += success.failed
              }
            });
            res.errors.forEach(function(err){
              if (!err) {
                return;
              }
              if (!angular.isDefined(err.failed)){
                numOfFailed += err.failed;
              }
            });

            if (numOfSuccessful > 0){
              myAppPopup.open({
                title: "Success",
                body: "There were a total of " + numOfSuccessful + " messages delivered successfully and a total of "+ numOfFailed + " failed deliveries"
              });
            }else{
              myAppPopup.alert({
                title: "Oops!",
                body: "None of the messages were delivered successfully"
              });
            }

            myAppProgress.complete();
          })
          .catch(function(err){
            console.log(err);
            myAppProgress.complete();
            myAppPopup.alert({
              title: "Oops!",
              body: "There was an error in sending the message"
            });
            myAppProgress.complete();
          })
      }
    }])
})();
