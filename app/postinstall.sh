#!/usr/bin/env bash

HOME=~/.electron-gyp

VERSION=$(electron -v)

cd node_modules/leveldown
../../../node_modules/.bin/node-gyp rebuild --target=1.3.5 --arch=x64 --dist-url=https://atom.io/download/atom-shell

cd ../sqlite3
../../../node_modules/.bin/node-gyp configure --module_name=node_sqlite3 --module_path=../lib/binding/electron-v1.3-linux-x64
../../../node_modules/.bin/node-gyp rebuild --target=1.3.5 --arch=x64 --dist-url=http://electron.atom.io/ --module_name=node_sqlite3 --module_path=../lib/binding/electron-v1.3-linux-x64

