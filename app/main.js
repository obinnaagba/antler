/**
 * Created by teliov on 12/27/16.
 */
var electron = require('electron');
var electronDebug = require('electron-debug');
var contextMenu = require('electron-context-menu');


electronDebug({
  showDevTools: true
});

contextMenu({

});

var app = electron.app;
var BrowserWindow = electron.BrowserWindow;
var ipcMain = electron.ipcMain;


var mainWindow = null;
var printWindow = null;

app.on('ready', function() {
  mainWindow = new BrowserWindow({
    width: 1024,
    height: 768,
    minWidth: 800,
    minHeight: 600

  });

  mainWindow.loadURL('file://' + __dirname + '/index.html');
});

ipcMain.on("print-command", function(event, arg){
  if (!printWindow){
    printWindow = new BrowserWindow({parent: mainWindow, modal: true, show: false, backgroundColor: '#2e2c29'})
  }

  printWindow.webContents.insertCSS("page-break-before: always");

  var filePath = require('url').format({
    protocol: 'file',
    slashes: true,
    pathname: arg
  })

  printWindow.loadURL(filePath);


  printWindow.webContents.on('did-finish-load', function(){
    printWindow.show();

    printWindow.webContents.print({}, function(err, res){
      console.log(arguments);
    })
  });



  printWindow.on('close', function(){
    printWindow = null;
  })
});
