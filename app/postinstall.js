var os = require("os");
var exec = require("child_process").exec;
var path = require("path");

var cmd = null;
var platform = os.platform();
var architecture = systemArchitecture();


function systemArchitecture(){
  var arch = process.arch;
  if (/32/.test(arch)){
    return 32;
  }

  if (/64/.test(arch)){
    return 64;
  }

  return null;
}

switch (architecture){
  case 64:
    cmd = "bash postinstall.sh";
    break;
  case 32:
    cmd = "bash postinstall_32.sh";
    break;
}

exec(cmd);
